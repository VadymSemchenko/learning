# ProCoders PHP Learning course

## Базовые видео курсы

[Дерево обучения](https://edu.cbsystematics.com/Images/RoadMap/Roadmap_Frontend-min.jpg)


### 1. PHP

### 2. Общие курсы по экосистеме PHP

* Понимаем современные фреймворки с курсом [PSR-7 framework](https://elisdn.ru/blog/113/psr7-framework-http)
* [Как писать код](https://github.com/roistat/php-code-conventions)

Слушать видно курсы можно на скорости х1.25 и даже разогнаться до х1.75. Используйте [VLC player](https://www.videolan.org/index.ru.html)


### 3. Laravel framework

* Наиболее полный и современный [курс Laravel](https://nnm-club.me/forum/viewtopic.php?t=1217186) пишем сайт объявлений на Laravel от Дмитрий Елисеев
* Книга [Паттерны в Laravel](https://yadi.sk/i/qlC960zY1-ibLw)
* Разработка через тестирование [Test-Driven Laravel](https://coursehunters.net/course/adamwathan-test-driven-laravel)

#### 3.3 Lararvel tools

* Интерактивная консоль [Laravel Tinker](https://laravel-news.com/laravel-tinker)

### 4. Yii Framework

Официальный сайт [Yii](https://www.yiiframework.com/)
* [Блог Yii2](https://elisdn.ru/blog/tag/Yii2) желательно прочесть все статьи
* Книга [Yii2 Cookbook](https://rutracker.org/forum/viewtopic.php?t=5314755)
