# ProCoders Angular Learning course

## Базовые видео курсы

[Дерево обучения](https://edu.cbsystematics.com/Images/RoadMap/Roadmap_Frontend-min.jpg)


### 1. Javascript
* [Learn JS for dummies](http://www.learn-js.org/)
* разобраться в деталях ES2015 на основе [презентации](http://courseware.codeschool.com.s3.amazonaws.com/es2015-the-shape-of-javascript-to-come/all-levels.pdf) знать досконально!

### 2. Пишем на Typescript
разобраться с [TypeScript за 5 минут](https://www.typescriptlang.org/docs/handbook/typescript-in-5-minutes.html)
и с [Typescript OOP](https://www.typescriptlang.org/docs/handbook/basic-types.html)


### 2. Видео-курсы Angular

Слушать видно курсы можно на скорости х1.25 и даже разогнаться до х1.75. Используйте [VLC player](https://www.videolan.org/index.ru.html)
* Самый полный курс 28 часов на английском языке от Udemy [Angular 6. The complete guide](https://rutracker.org/forum/viewtopic.php?t=5534170)
* Более краткий мастер-класс от Udemy на 15+ часов [TypeScript, Angular, Firebase & Angular Material Masterclass](https://rutracker.org/forum/viewtopic.php?t=5548685)

Если же с английским язком совсем "тяжело", тогда качаем и смотрим для убогих
* [Angular 4 с Нуля до Профи](https://rutracker.org/forum/viewtopic.php?t=5471796)
* [Продвинутый курс Angular 6](https://coursehunters.net/course/prodvinutyy-kurs-po-angular) от learn.javascript.ru


### 4. Angular base patterns
* [Styleguide от лысого из браззерс](https://angular.io/guide/styleguide)
* [Best pracs для новичков](https://codeburst.io/angular-best-practices-4bed7ae1d0b7)
* Книга на английском [Angular Design Patterns](https://yadi.sk/d/p1ytQtXTbJTIyw)

#### 4.1 RxJS
RxJS - ключевая библиотека Angular
* [Что такое RxJS](https://habr.com/company/ruvds/blog/341880/)

#### 4.2 Angular Store pattern via NGRX
* Статья в трех частях [Реактивные приложения на Angular](https://medium.com/@demyanyuk/%D1%80%D0%B5%D0%B0%D0%BA%D1%82%D0%B8%D0%B2%D0%BD%D1%8B%D0%B5-%D0%BF%D1%80%D0%B8%D0%BB%D0%BE%D0%B6%D0%B5%D0%BD%D0%B8%D1%8F-%D0%BD%D0%B0-angular-ngrx-%D1%87%D0%B0%D1%81%D1%82%D1%8C-1-cb7b4f2852dc)

### 5. Тестироваие в Angular
* Статья о тестировании [AngularTesting Guide v4+](https://medium.com/google-developer-experts/angular-2-testing-guide-a485b6cb1ef0)
* Видеокурс [Play by Play: Fundamentals of Angular Testing](https://rutracker.org/forum/viewtopic.php?t=5588880)

### 6. Коммуникации с сервисами
* Работа с Firebase в Angular [Building a Realtime Chat Application with Angular and Firebase](https://rutracker.org/forum/viewtopic.php?t=5548675)

### 7. Ionic
* Видео курс Ionic3 + backend [Криптовалютное приложение с backend-ом](https://coursehunters.net/course/ionic-3-sozdaem-krasivoe-kriptovalyutnoe-prilozhenie)
* Лепим простенький калькулятор по статье [Getting started with Ionic](https://medium.com/craft-academy/getting-started-with-ionic-5a05b1ba8293)
* Делаем более сложное приложение по статье [Создание приложения на Ionic с использованием API](https://habr.com/post/344474/)

### 8. Утилиты и библиотеки
* [Json-Server](https://medium.com/codingthesmartway-com-blog/create-a-rest-api-with-json-server-36da8680136d) для быстрого мока бекенда 
* [Мышление в стиле Ramda: Первые шаги](https://habr.com/post/348868/)

### 9. GraphQL
* [Что такое GraphQL](https://habr.com/post/326986/)
* [Переход от REST API к GraphQL](https://habr.com/post/334182/)
* [Complete video Tutorial for GraphQL](https://www.howtographql.com/)

Смотреть инфо по Apollo и связи с Angular

### 9. Server Rendering Angular

## Общие скиллы

### 1. VsCode
* [Tips and Tricks for VSCode](https://github.com/Microsoft/vscode-tips-and-tricks)

### 2. Принципы программирования
* [Паттерны](https://www.youtube.com/watch?v=Z90DFL2Ndow)
* [SOLID принципы](https://www.youtube.com/watch?v=59tq5Fcgn7A)
