# ProCoders React Native Learning course

Изначально необходимо знать и разбираться в [React](./react.md)

## Базовые видео курсы

Общие ссылки:
* Новейшая видео обучалка React Native + GraphQL[Building AirBnb Clone](https://www.youtube.com/playlist?list=PLN3n1USn4xlnfJIQBa6bBjjiECnk6zL6s). Крайне рекомендовано к просмотру как для изучения React Native так и для React Apollo! 
* [Official tutorial](https://facebook.github.io/react-native/docs/tutorial.html)
* Собрание обучалок [Tutorials React Native](https://proglib.io/p/react-native-tutorials/)
* [Video React Native - The Practical Guide](https://coursehunters.net/course/react-native-prakticheskoe-rukovodstvo)
* React Native: Продвинутые концепции [React Native: Advanced Concepts](https://coursehunters.net/course/react-native-prodvinutye-koncepcii)
* Руководство по Анимациям в React Native [Master React Native Animations](https://coursehunters.net/course/rukovodstvo-po-animaciyam-v-react-native)
* React Native Design, включая Native Base (см. урок 30 и далее) [React Native Design - Build front end of 10 mobile Apps](https://coursehunters.net/course/react-native-design-delaem-front-end-10-ti-mobilnym-prilozheniyam)

### 1. Общие статьи 
* Быстрое введение-обзор в [React Native одного JS мало](https://habr.com/post/323214/)
* [React Native с точки зрения мобильного разработчика](https://habr.com/company/qlean/blog/416097/) позволяет более глубже понять мир мобильных приложений
* изучить список плагинов для работы с периферией [Awesome React Native](https://github.com/jondot/awesome-react-native#system)
* ознакомиться с UI компонентами, чтобы не писать велосипед [Awesome React Native UI](https://github.com/madhavanmalolan/awesome-reactnative-ui)
* ументь поддержать holly-war [Ionic 2 vs React Native: сравнение фреймворков](https://habr.com/post/328960/)

### 2. Boilerplates & StarterKits

Всегда помним, что не нужно собирать каждый раз с нуля все руками. За вас это уже сделали!!!

### 2.1 Ignite CLI

Самый модный бойлер с консольным генератором [ignite CLI](https://infinite.red/ignite)
* классика (ignite + redux) https://github.com/infinitered/ignite-ir-boilerplate-andross
* или ignite + native base https://github.com/GeekyAnts/ignite-native-base-boilerplate
* или добавим https://react-native-training.github.io/react-native-elements

добираем [плагины ignite](https://github.com/infinitered/ignite/blob/master/PLUGINS.md) : firebase, img-cache, redux-devtools, redux-persist 

### 3. Плагины и компоненты
* [NativeBase](https://nativebase.io/) как основные компоненты для приложения
* [ReactNative Firebase](https://rnfirebase.io/) как важная часть инфраструктуры приложения. Разбираться с: Cloud Messaging, Crashlytics, Dynamic Links, 
* важные плагины react-native-firebase, react-native-permissions, react-native-notifications, react-native-background-fetch, react-native-touch-id, react-native-in-app-utils, react-native-barcodescanner react-native-location, react-native-social-share, react-native-motion-manage, react-native-android-sms-listener, react-native-mauron85-background-geolocation

### 4. Специфика и нативность
* Пишем собственный нативный модуль [...все возможности мобильной ОС в React Native](https://habr.com/company/epam_systems/blog/347346/)
* Особенности настройки React Native на Android[Debugging common React Native issues on Android](https://blog.pusher.com/debugging-react-native-android/) 

### 5. Маршрутизация в React Native
 * Обзор роутеров в React Native [Navigating Navigation in React Native](https://medium.com/handlebar-labs/navigating-navigation-in-react-native-26c7e4690f94)
 * Наиболее глубоко нативно-интегрированный роутер[React Native Navigation (V2) by Wix — Getting Started](https://medium.com/react-native-training/react-native-navigation-v2-by-wix-getting-started-7d647e944132)
 * Статья с видеоуроками по [React Native Basics: Using react-native-router-flux](https://medium.com/differential/react-native-basics-using-react-native-router-flux-f11e5128aff9)

### 6. Deploy и заливка
* [CodePush](https://github.com/Microsoft/react-native-code-push) для обновления приложений без документации