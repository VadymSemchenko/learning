## Strapi Framework

[Strapi](https://strapi.io/) - новый интересный фремворк для Node.js для быстрого прототипирования API

Посмотрите [Strapi Demo](https://strapi.io/demo) и становится понятна суть это framework

### Понимание

+ Use Strapi with docker-compose [Strapi docker](https://github.com/strapi/strapi-docker/blob/master/docker-compose.yml)
+ [Strapi getting started](https://strapi.io/getting-started)
+ [Official documentation](https://strapi.io/documentation/concepts/concepts.html)

### Как писать Frontend часть
+ [React + Apollo](https://github.com/strapi/strapi-examples/tree/master/react-apollo)
+ [Vue + Nuxt + GraphQl](https://blog.strapi.io/cooking-a-deliveroo-clone-with-nuxt-vue-js-graphql-strapi-and-stripe-restaurants-list-part-2-7/)

